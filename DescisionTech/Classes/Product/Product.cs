﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DescisionTech
{
    public class Product
    {
        public Product(string Name, decimal Cost)
        {
            this.Name = Name;
            this.Cost = Cost;
        }

        public string Name { get; set; }

        public decimal Cost { get; set; }
    }
}
