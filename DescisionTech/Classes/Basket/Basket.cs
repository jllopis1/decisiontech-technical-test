﻿using System;
using System.Collections.Generic;
using System.Linq;
using static DescisionTech.Discount;

namespace DescisionTech
{
    public class Basket 
    {

        private List<Product> Products = new List<Product>();
    
        public void AddProduct(Product product)
        {

            if (CheckType(product))
            {
                Products.Add(product);
            }
           
        }
  
        public decimal GetPrice()
        {
            return Discount.GetPrice(this.Products);
        }


        public bool CheckType(Product product)
        {
            return (product.Name == ProductType.Milk.ToString() || product.Name == ProductType.Bread.ToString() || product.Name == ProductType.Butter.ToString());
            
        }

    }
}
