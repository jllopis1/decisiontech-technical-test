﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DescisionTech
{
    public class Discount
    {
       
        public static decimal CounterProducts(List<Product> products, string key)
        {
            return products.Where(p => p.Name.Equals(key)).Count();
        }

        public static decimal GetPrice(List<Product> products)
        {
            decimal counterMilk = CounterProducts(products,ProductType.Milk.ToString());
            decimal counterBread = CounterProducts(products, ProductType.Bread.ToString());
            decimal counterButter = CounterProducts(products, ProductType.Butter.ToString());

            decimal discBread = Math.Floor(counterButter / 2.0m);
            decimal discMilk = Math.Floor(counterMilk / 4.0m);

            decimal costTotal = GetCostProduct(products, ProductType.Butter.ToString());

            if(counterBread <= discBread)
            {
                costTotal += GetCostProduct(products, ProductType.Bread.ToString()) * 0.5m;

            } else
            {
                decimal priceBread = GetPriceProduct(products, ProductType.Bread.ToString());
                costTotal += (discBread * 0.5m * priceBread) + (counterBread-discBread) * priceBread;
            }

            decimal priceMilk = GetPriceProduct(products, ProductType.Milk.ToString());
            costTotal += (counterMilk - discMilk) * priceMilk;

            return costTotal;
        }

        public static decimal GetCostProduct(List<Product> products, string key)
        {
            decimal cost = 0m;
            foreach(Product product in products)
            {
             if (product.Name == key)
                {
                    cost += product.Cost;
                }  
            }
            return cost;
        }

        public static decimal GetPriceProduct(List<Product> products, string key)
        {
            decimal cost = 0m;
            foreach (Product product in products)
            {
                if (product.Name == key)
                {
                    cost = product.Cost;
                    break;
                }
            }
            return cost;
        }
    }
}
