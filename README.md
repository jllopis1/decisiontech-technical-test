﻿# Price calculation exercise

DecisionTech Aplication test : 2 class Library , DecisionTech & DecisionTech.Test

## Tests

- Basket_Empty()
- Product_Name_Not_Valid()
- Scenario_1()
- Scenario_2()
- Scenario_3()
- Scenario_4()


## Classes

- Discount
- Basket
- Product
- ProductType(Enum)


## Git Repository

https://gitlab.com/jllopis1/decisiontech-technical-test
