﻿using DescisionTech;
using System;
using System.Collections.Generic;
using Xunit;


namespace DecisionTechTest
{
    public class ProductTest 
    {
        private readonly Product _milk;
        private readonly Product _butter;
        private readonly Product _bread;

        public ProductTest()
        {
            _milk = new Product("Milk", 1.15m);
            _butter = new Product("Butter", 0.80m); 
            _bread = new Product("Bread", 1m);
        }

       
        [Fact]
        public void Basket_Empty()
        {

            //Arrange 
            List<Product> productList = new List<Product>();

            //Assert
            Assert.Empty(productList);

        }

        [Fact]
        public void Product_Name_Not_Valid()
        {

            //Arrange 
            var basket = new Basket();
            Product product = new Product("WrongName", 1m);
            

            //Act
            basket.AddProduct(product);

            //Assert
            Assert.Equal(0, 0);

        }


        [Fact]
        public void Scenario_1()
        {

            //Arrange
            var basket = new Basket();

            //Act
            basket.AddProduct(_milk);
            basket.AddProduct(_butter);
            basket.AddProduct(_bread);

            //Assert
            Assert.Equal(2.95m, basket.GetPrice());

        }


        [Fact]
        public void Scenario_2()
        {

            //Arrange 
            var basket = new Basket();
            
            //Act
            basket.AddProduct(_butter);
            basket.AddProduct(_butter);
            basket.AddProduct(_bread);
            basket.AddProduct(_bread);

            //Assert
            Assert.Equal(3.10m, basket.GetPrice());

        }


        [Fact]
        public void Scenario_3()
        {

            //Arrange 
            var basket = new Basket();

            //Act
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);

            //Assert
            Assert.Equal(3.45m, basket.GetPrice());

        }


        [Fact]
        public void Scenario_4()
        {

            //Arrange 
            var basket = new Basket();

            //Act
            basket.AddProduct(_butter);
            basket.AddProduct(_butter);
            basket.AddProduct(_bread);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);
            basket.AddProduct(_milk);

            //Assert
            Assert.Equal(9.0m, basket.GetPrice());

        }
    }
}
